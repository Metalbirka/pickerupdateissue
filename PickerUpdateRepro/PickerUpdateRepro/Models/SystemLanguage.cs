﻿using PickerUpdateRepro.Enums;

namespace PickerUpdateRepro.Models
{
    public class SystemLanguage
    {
        public LanguageCode LanguageCode { get; set; }
        public string DisplayName { get; set; }
    }
}
