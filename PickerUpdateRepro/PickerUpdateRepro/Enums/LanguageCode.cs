﻿namespace PickerUpdateRepro.Enums
{
    public enum LanguageCode
    {
        EN,
        HU,
        FR
    }
}