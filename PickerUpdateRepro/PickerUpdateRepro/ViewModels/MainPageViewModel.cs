﻿using PickerUpdateRepro.Enums;
using PickerUpdateRepro.Models;
using Prism.Navigation;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace PickerUpdateRepro.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private SystemLanguage _initialLanguage;
        private SystemLanguage _selectedLanguage;
        private ObservableCollection<SystemLanguage> _languagesList;
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
            LanguagesList = new ObservableCollection<SystemLanguage>();
            SelectedIndexChangedCommand = new Command(SelectedIndexChanged);
        }

        private void SelectedIndexChanged(object obj)
        {
            Debug.WriteLine("Command was called.");

            if (SelectedLanguage.LanguageCode != LanguageCode.HU) return;

            foreach (var item in LanguagesList)
            {
                item.DisplayName = item.LanguageCode == LanguageCode.EN ? "Angol" : "Magyar";
                //RaisePropertyChanged("LanguagesList"); //not working
            }

            //This is going to crash, but I need to give the list a new source since it will trigger only the "Update" event of the picker's list
            //LanguagesList = InitializeHungarianLanguage();

            Debug.WriteLine("In theory after 'modification' the correct value should be visible");
            LanguagesList.ToList().ForEach(_ => Debug.WriteLine(_.DisplayName));
        }

        public ICommand SelectedIndexChangedCommand { get; }

        public SystemLanguage InitialLanguage
        {
            get => _initialLanguage;
            set
            {
                _initialLanguage = value;
                RaisePropertyChanged();
            }
        }

        public SystemLanguage SelectedLanguage
        {
            get => _selectedLanguage;
            set
            {
                Debug.WriteLine($"SelectedLanguage - PropertyChanged() {value.DisplayName}");
                _selectedLanguage = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<SystemLanguage> LanguagesList
        {
            get => _languagesList;
            set
            {
                Debug.WriteLine($"LanguagesList - PropertyChanged() {value}");
                _languagesList = value;
                RaisePropertyChanged();
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);

            LanguagesList = InitializeLanguage();

            // Printing out the initial list
            Debug.WriteLine("Initial list: \n");
            LanguagesList.ToList().ForEach(_ => Debug.WriteLine(_.DisplayName));

            InitialLanguage = LanguagesList.Where(_ => _.LanguageCode == LanguageCode.EN).FirstOrDefault();
        }

        public ObservableCollection<SystemLanguage> InitializeLanguage()
        {
            var list = new ObservableCollection<SystemLanguage>
            {
                new SystemLanguage{ LanguageCode= LanguageCode.EN, DisplayName = "English"},
                new SystemLanguage{ LanguageCode= LanguageCode.HU, DisplayName = "Hungarian"},
            };

            return list;
        }

        //easier than typing
        public ObservableCollection<SystemLanguage> InitializeHungarianLanguage()
        {
            var list = new ObservableCollection<SystemLanguage>
            {
                new SystemLanguage{ LanguageCode= LanguageCode.EN, DisplayName = "Angol"},
                new SystemLanguage{ LanguageCode= LanguageCode.HU, DisplayName = "Magyar"},
            };

            return list;
        }
    }
}
